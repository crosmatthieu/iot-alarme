APPLICATION = ALARME
RIOTBASE ?= $(CURDIR)/../RIOT-OS/RIOT
include $(CURDIR)/../RIOT-OS/RIOT/tests/Makefile.tests_common


FEATURES_REQUIRED += periph_gpio
FEATURES_OPTIONAL += periph_gpio_irq
FEATURES_OPTIONAL += periph_gpio_fast_read
FEATURES_OPTIONAL += periph_gpio_tamper_wake


# avoid running Kconfig by default
SHOULD_RUN_KCONFIG ?=


USEMODULE += xtimer

BOARD ?= wyres-base


USEMODULE += od
USEMODULE += ps

DRIVER ?= sx1272

# use SX1276 by default
USEMODULE += $(DRIVER)



include $(RIOTBASE)/Makefile.include

